package com.seleniumframework.listeners;

import com.seleniumframework.DriverBase;
import com.seleniumframework.testrail.APIClient;
import com.seleniumframework.testrail.APIException;
import com.seleniumframework.tests.BasicIT;
import io.qameta.allure.Attachment;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.logging.LogEntries;
import org.openqa.selenium.logging.LogEntry;
import org.openqa.selenium.logging.LogType;
import org.openqa.selenium.remote.Augmenter;
import org.testng.Assert;
import org.testng.IClass;
import org.testng.ITestResult;
import org.testng.TestListenerAdapter;
import simple.JSONObject;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

import static com.seleniumframework.DriverBase.getDriver;


public class ScreenshotListener extends TestListenerAdapter {
    public static String TEST_RUN_ID                = System.getProperty("testrailid");
    public static String TESTRAIL_USERNAME          = "andres.cruz@gm2dev.com";
    public static String TESTRAIL_PASSWORD          = "505864AnderesuRama123";
    public static String RAILS_ENGINE_URL           = "https://airmapio.testrail.com";
    public static final int TEST_CASE_PASSED_STATUS = 1;
    public static final int TEST_CASE_FAILED_STATUS = 5;
    private final boolean isUsingTestRail = Boolean.getBoolean("useTestRail");

    @Attachment(value = "Page screenshot", type = "image/png")
    public byte[] saveScreenshotPNG (WebDriver driver) {
        return ((TakesScreenshot)driver).getScreenshotAs(OutputType.BYTES);
    }



    //Text attachments for Allure
    @Attachment(value = "Browser Log", type = "text/plain")
    public String saveTextLog () throws Exception{
        String filename = "proxyLog" + System.currentTimeMillis() + ".txt";
        BufferedWriter writer = new BufferedWriter(new FileWriter(filename));
        LogEntries logEntries = DriverBase.getDriver().manage().logs().get(LogType.BROWSER);
        String asd = logEntries.getAll().toString();
        return asd;
    }



    private boolean createFile(File screenshot) {
        boolean fileCreated = false;

        if (screenshot.exists()) {
            fileCreated = true;
        } else {
            File parentDirectory = new File(screenshot.getParent());
            if (parentDirectory.exists() || parentDirectory.mkdirs()) {
                try {
                    fileCreated = screenshot.createNewFile();
                } catch (IOException errorCreatingScreenshot) {
                    errorCreatingScreenshot.printStackTrace();
                }
            }
        }

        return fileCreated;
    }

    private void writeScreenshotToFile(WebDriver driver, File screenshot) {
        try {
            FileOutputStream screenshotStream = new FileOutputStream(screenshot);
            screenshotStream.write(((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES));
            screenshotStream.close();
        } catch (IOException unableToWriteScreenshot) {
            System.err.println("Unable to write " + screenshot.getAbsolutePath());
            unableToWriteScreenshot.printStackTrace();
        }
    }


    @Override
    public void onTestSuccess(ITestResult result) {
        APIClient client = new APIClient(RAILS_ENGINE_URL);
        client.setUser(TESTRAIL_USERNAME);
        client.setPassword(TESTRAIL_PASSWORD);
        Map data = new HashMap();
        data.put("status_id", TEST_CASE_PASSED_STATUS);
        data.put("comment", "Test Executed - Status updated automatically by Selenium");
        String TestID=null;
        IClass obj = result.getTestClass();
        Class newobj = obj.getRealClass();
        Method testMethod = null;
        try { testMethod = newobj.getMethod(result.getName()); }
        catch (NoSuchMethodException e) { e.printStackTrace(); }
        if (testMethod.isAnnotationPresent(BasicIT.UseAsTestRailId.class))
        {
            BasicIT.UseAsTestRailId useAsTestName = testMethod.getAnnotation(BasicIT.UseAsTestRailId.class);
            TestID = Integer.toString(useAsTestName.testRailId());
            System.out.println("Test Rail ID = " + TestID);
        }
        if(isUsingTestRail) {
            try { JSONObject r = (JSONObject) client.sendPost("add_result_for_case/"+TEST_RUN_ID+"/"+ TestID +"",data); }
            catch (IOException e) { System.out.println(e); }
            catch (APIException e) { System.out.println(e); }
        } else {
            System.out.println("Not using test rail");
        }
    }






    //@Attachment(value = "Page screenshot", type = "image/png")
    @Override
    public void onTestFailure(ITestResult failingTest) {
        try {
            WebDriver driver = getDriver();
            String screenshotDirectory = System.getProperty("screenshotDirectory", "target/screenshots");
            String screenshotAbsolutePath = screenshotDirectory + File.separator + System.currentTimeMillis() + "_" + failingTest.getName() + ".png";
            File screenshot = new File(screenshotAbsolutePath);
            if (createFile(screenshot)) {
                try {
                    writeScreenshotToFile(driver, screenshot);
                    saveScreenshotPNG(driver);
                    saveTextLog();
                    System.out.println(System.getProperty("jenkinsURL") + "/job/SFO/ws/target/screenshots/");
                } catch (ClassCastException weNeedToAugmentOurDriverObject) {
                    writeScreenshotToFile(new Augmenter().augment(driver), screenshot);
                }
                System.out.println("Written screenshot to " + screenshotAbsolutePath);
            } else {
                System.err.println("Unable to create " + screenshotAbsolutePath);
            }
        } catch (Exception ex) {
            System.err.println("Unable to capture screenshot: " + ex.getCause());

        }
        APIClient client = new APIClient(RAILS_ENGINE_URL);
        client.setUser(TESTRAIL_USERNAME);
        client.setPassword(TESTRAIL_PASSWORD);
        Map data = new HashMap();
        data.put("status_id", TEST_CASE_FAILED_STATUS);
        data.put("comment", "Test Executed - Status updated automatically by Selenium");
        String TestID=null;
        IClass obj = failingTest.getTestClass();
        Class newobj = obj.getRealClass();
        Method testMethod = null;
        try { testMethod = newobj.getMethod(failingTest.getName()); }
        catch (NoSuchMethodException e) { e.printStackTrace(); }
        if (testMethod.isAnnotationPresent(BasicIT.UseAsTestRailId.class))
        {
            BasicIT.UseAsTestRailId useAsTestName = testMethod.getAnnotation(BasicIT.UseAsTestRailId.class);
            TestID = Integer.toString(useAsTestName.testRailId());
            System.out.println("Test Rail ID = " + TestID);
        }
        if(isUsingTestRail) {
            try { JSONObject r = (JSONObject) client.sendPost("add_result_for_case/"+TEST_RUN_ID+"/"+TestID+"",data); }
            catch (IOException e) { System.out.println(e); }
            catch (APIException e) { System.out.println(e); }
        } else {
            System.out.println("Not using test rail");
        }
    }
}