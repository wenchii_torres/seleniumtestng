package com.seleniumframework.page_objects;

import com.seleniumframework.tests.LogUtil;
import org.openqa.selenium.*;

import com.seleniumframework.DriverBase;

import org.openqa.selenium.logging.LogEntries;
import org.openqa.selenium.logging.LogEntry;
import org.openqa.selenium.logging.LogType;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import java.io.*;

public class PageActions extends BasePage{

    public static By searchBox = By.name("q");


    public static void searchInGoogle(String value, WebDriver driver) throws Exception{
        WebDriverWait wait = new WebDriverWait(DriverBase.getDriver(), 15, 100);
        sendKeys(searchBox,value);
        LogUtil.log("Searching " + value + "...");
        wait.until(ExpectedConditions.visibilityOfElementLocated(searchBox));
        driver.findElement(searchBox).sendKeys(Keys.RETURN);
    }

    public static void writeFile(final String filename, final String text) {
        System.out.println(text);
        try {
            PrintWriter out = new PrintWriter(filename);
            out.println(text);
            out.close();
            //you are trying to write
        } catch (IOException e) {
            System.out.println("Exception ");

        }
    }

    public static void writeEnvironment( String filename,  String Arg1, String Arg2 ) throws Exception{
        BufferedWriter writer = new BufferedWriter(new FileWriter(
                filename));
        // Write these lines to the file.
        // ... We call newLine to insert a newline character.
        writer.write(Arg1);
        writer.newLine();
        writer.write(Arg2);
        writer.newLine();
        writer.close();
    }

    public static void writeJsLogs(String filename) throws Exception{
        BufferedWriter writer = new BufferedWriter(new FileWriter(
                filename));
        LogEntries logEntries = DriverBase.getDriver().manage().logs().get(LogType.BROWSER);
        for (LogEntry logEntry: logEntries) {
            writer.write(logEntry.toString());
            writer.newLine();
        }
        writer.close();
    }

    public static void validatePageTitle(String title, WebDriver driver){
        LogUtil.log("Checking page title . . .");
        System.out.println("Page title is: " + driver.getTitle());
        String pageTitle = driver.getTitle();
        Assert.assertTrue(pageTitle.contains(title), "the title of the google search value is different");
    }

    public static void sendKeysForUpload(final By locator, final String value, WebDriver driver) {
        final WebDriverWait wait = new WebDriverWait(driver, 10, 100);
        final WebElement element = wait.until(ExpectedConditions.presenceOfElementLocated(locator));
        element.sendKeys(value);
    }

    public static void clickElementJs(By locator) throws Exception{
        JavascriptExecutor executor = (JavascriptExecutor)DriverBase.getDriver();
        WebDriverWait wait = new WebDriverWait(DriverBase.getDriver(), 10, 100);
        WebElement element = wait.until(ExpectedConditions.elementToBeClickable(locator));
        executor.executeScript("arguments[0].click();", element);
    }

    public static void sendKeys(By locator, String value) throws Exception {
        WebDriverWait wait = new WebDriverWait(DriverBase.getDriver(), 10, 100);
        WebElement element = wait.until(ExpectedConditions.presenceOfElementLocated(locator));
        wait.until((ExpectedConditions.elementToBeClickable(locator)));
        LogUtil.log("sending keys with value: " + value + " to locator: " + locator);
        element.sendKeys(value);
    }

    public static void clickElement(By locator) throws  Exception{
        WebDriverWait wait = new WebDriverWait(DriverBase.getDriver(), 10, 100);
        WebElement element = wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
        wait.until((ExpectedConditions.elementToBeClickable(locator)));
        LogUtil.log("clicking element by locator: " + locator);
        element.click();
    }

    public static int countElementByXpath(String xpath) throws Exception{
        WebDriverWait wait = new WebDriverWait(DriverBase.getDriver(), 10, 100);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[contains(@class, 'tableOverflowWrapper')]")));
        LogUtil.log("counting operations ... ");
        int locatorElementSize = DriverBase.getDriver().findElements(By.xpath(xpath)).size();
        return locatorElementSize;
    }
}