package com.seleniumframework.tests;

import com.seleniumframework.DriverBase;
import com.seleniumframework.page_objects.PageActions;
import io.qameta.allure.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.net.MalformedURLException;
import java.util.Properties;

public class BasicIT extends DriverBase {

    private RemoteWebDriver driver;

    @Retention(RetentionPolicy.RUNTIME)
    @Target(ElementType.METHOD)
    public @interface UseAsTestRailId
    {
        int testRailId() default 0;
        String[] tags() default "";
    }

    @BeforeMethod
    public void setup() throws MalformedURLException {
        driver = getDriver();
        driver.manage().window().maximize();
    }

    private String setEnvironment(String url, Properties properties){
        String env = System.getProperty("environment");
        String openThisURL;
        if (env != null) {
            switch (env) {
                case "prod":
                    openThisURL = properties.getProperty(url.toLowerCase() + ".url.prod");
                    System.out.println("Environment: " + properties.getProperty(url + ".url.prod"));
                    break;
                case "dev":
                    openThisURL = properties.getProperty(url.toLowerCase() + ".url.dev");
                    System.out.println("Environment: " + properties.getProperty(url + ".url.dev"));
                    break;
                case "test":
                    openThisURL = properties.getProperty(url.toLowerCase() + ".url.test");
                    System.out.println("Environment: " + properties.getProperty(url + ".url.test"));
                    break;
                case "stage":
                    openThisURL = properties.getProperty(url.toLowerCase() + ".url.stage");
                    System.out.println("Environment: " + properties.getProperty(url + ".url.stage"));
                    break;
                default:
                    openThisURL = properties.getProperty(url.toLowerCase() + ".url.prod");
                    System.out.println("Environment: " + properties.getProperty(url + ".url.dev"));
                    break;
            }
            System.out.println("Loaded env: " + env);
        } else {
            openThisURL = properties.getProperty(url.toLowerCase() + ".url.stage");
            System.out.println("Environment: " + properties.getProperty(url.toLowerCase() + ".url.stage"));
        }
        return openThisURL;
    }

    private Properties setProperties() {
        InputStream input = null;
        Properties properties = new Properties();
        try {
            input = new FileInputStream(System.getProperty("user.dir") + "/all.properties");
            properties.load(input);
            System.out.println("properties file path: " + System.getProperty("user.dir") + "/all.properties");

        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } return properties;
    }

    private void googleExampleThatSearchesFor(final String searchString) throws Exception {
        Properties properties = setProperties();
        WebDriver driver = DriverBase.getDriver();
        LogUtil.log("Opening browser.");
        driver.get(setEnvironment("google", properties));
        PageActions.searchInGoogle(searchString, driver);
        PageActions.validatePageTitle(searchString, driver);
    }


    @Feature("GOOGLE DEMO")
    @Test(description = "Google Example Number 1")
    @Description("This tests is an example for the test suite")
    @Severity(SeverityLevel.TRIVIAL)
    public void googleExample1() throws Exception {
        googleExampleThatSearchesFor("Welcome to Automation!");
    }

    @Feature("GOOGLE DEMO")
    @Test(description = "Google Example Number 2")
    @Description("This test is an example for the test suite. trivial")
    @Severity(SeverityLevel.TRIVIAL)
    public void googleExample2() throws Exception {
        googleExampleThatSearchesFor("Selenium webdriver");
    }

    @Feature("GOOGLE DEMO")
    @Test(description = "Google Example Number 3")
    @Description("This test is an example for the test suite. normal")
    @Severity(SeverityLevel.NORMAL)
    public void googleExample3() throws Exception {
        googleExampleThatSearchesFor("TESTNG And SELENIUM");
    }

    @Feature("GOOGLE DEMO")
    @Test(description = "Google Example Number 4")
    @Description("This test is an example for the test suite. critical")
    @Severity(SeverityLevel.CRITICAL)
    public void googleExample4() throws Exception {
        googleExampleThatSearchesFor("Welcome to Automation!. the return of selenium");
    }

    @Feature("GOOGLE DEMO")
    @Test(description = "Google Example Number 5")
    @Description("This test is an example for the test suite. critical")
    @Severity(SeverityLevel.CRITICAL)
    public void googleExample5() throws Exception {
        googleExampleThatSearchesFor("Welcome to Automation!. the return of selenium");
    }

    @Feature("GOOGLE DEMO")
    @Test(description = "Google Example Number 6")
    @Description("This test is an example for the test suite. normal")
    @Severity(SeverityLevel.NORMAL)
    public void googleExample6() throws Exception {
        googleExampleThatSearchesFor("Welcome to Automation!. the return of selenium");
    }

    @Feature("GOOGLE DEMO")
    @Test(description = "Google Example Number 7")
    @Description("This test is an example for the test suite. minor")
    @Severity(SeverityLevel.MINOR)
    public void googleExample7() throws Exception {
        googleExampleThatSearchesFor("Welcome to Automation!. the return of selenium");
    }

    @Feature("GOOGLE DEMO")
    @Test(description = "Google Example Number 8")
    @Description("This test is an example for the test suite. trivial")
    @Severity(SeverityLevel.TRIVIAL)
    public void googleExample8() throws Exception {
        googleExampleThatSearchesFor("Welcome to Automation!. the return of selenium");
    }

    @Feature("GOOGLE DEMO")
    @Test(description = "Google Example Number 9")
    @Description("This test is an example for the test suite. BLOCKER")
    @Severity(SeverityLevel.BLOCKER)
    public void googleExample9() throws Exception {
        googleExampleThatSearchesFor("Welcome to Automation!. the return of selenium");
    }

    @Feature("GOOGLE DEMO")
    @Test(description = "Google Example Number Failed Test")
    @Description("This test is an example for the test suite. blocker")
    @Severity(SeverityLevel.BLOCKER)
    public void googleExampleFails() throws Exception {
        googleExampleThatSearchesFor("Failing!!!!");
        Assert.assertTrue(false);
    }

    @Feature("GOOGLE DEMO")
    @Test(description = "Google Example Number Flaky Test")
    @Description("This test is an example for the test suite. blocker")
    @Severity(SeverityLevel.BLOCKER)
    @UseAsTestRailId(testRailId = 16199)
    public void googleExampleFlacky() throws Exception {
        googleExampleThatSearchesFor("Failing!!!!");
        WebDriverWait wait = new WebDriverWait(DriverBase.getDriver(), 1, 100);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[contains(text(),'Accept')]")));
    }
}