# Welcome to  Automated Testing!  
  
This is the central repository of the AirMap Testing Suites.  
Unless otherwise noted, all code and configuration for running the AirMap Automated tests can be found here.   
  
Currently, we have tests made in Apache JMeter for testing the API
endpoints and tests made in selenium webdriver for SFO End to End
Testing. Both suites are in the this repository.  

* [Selenium Framework](#seleniumMain)
    * [What is the scope of the tests?](#seleniumScope)
    * [What do you need to run the tests locally?](#seleniumLocalReq)
    * [How to run the tests locally?](#seleniumLocalSteps)
    * [How to run the tests in jenkins?](#seleniumRemoteSteps)
    * [Where are the test code?](#seleniumTestCode)

* [JMeter API tests](#jmeterMain)
  *   [What is the scope of the tests?](#jmeterScope)
  *   [What do you need to run the tests locally?](#jmeterLocalReq)
  *   [How to run the tests locally?](#jmeterLocalSteps)
  *   [How to run the tests in jenkins?](#jmeterRemoteSteps)
  *   [Where are the test code?](#jmeterTestCode)
 

  
# Selenium Framework <a name="seleniumMain"></a>
  
We use **Apache Maven** for dependency management and for the structure
of the project. Maven uses a POM.xml file (project object model) where
you will be able to find all the dependencies and parameters that the
framework can receive:
  
Parameters:  
  
- environment (environment where you want to run the tests. the default value is "test")   
- browser (in which browser we want to run the tests, for now it can be done in chrome and firefox. is possible to add support for other browsers like safari, opera and edge. the default value is "chrome").    
- headless (if the browser is going to run with a graphic interface or not. Accepts true or false. the default value is false).  
- remote (false: runs the tests without selenium grid. this is going to user your installed browser to run the tests. true: runs the tests in a specific selenium grid, see next parameter. the default value is false).  
- seleniumGridURL (url to connect to a remote selenium grid. the default value is localhost:4444, which is the url of you local selenium grid container if you use the startDockerContainers.sh script in the /script path).  
- threads (number of threads to run the tests, each thread runs one tests, so you can run multiple tests at the same time. you can modify the startDockerContainers.sh script to add multiple selenium nodes to match the threads you want to run. the default value is '1').  
      
**Selenium WebDriver** is a collection of open source APIs which are used to automate the testing of a web application. We use this API's    
to communicate with browsers and perform actions on them.   
  
**Selenium Grid** is a part of the Selenium Suite that specializes in running multiple tests across different browsers, operating systems, and machines in parallel. The Airmap grid is located in http://selenium-hub.services.svc.cluster.local:4444/wd/hub  
  
We currently use selenium version 3.x (you can check the actual version in the POM.xml file   
Here you can check the last available and tested version:  
[https://mvnrepository.com/artifact/org.seleniumhq.selenium/selenium-java](https://mvnrepository.com/artifact/org.seleniumhq.selenium/selenium-java)  
    
We use **TestNG** as testing framework.  
Here you can check the last available and tested version:  
[https://mvnrepository.com/artifact/org.testng/testng](https://mvnrepository.com/artifact/org.testng/testng)  

We use **Allure Report** as report tool  
[https://docs.qameta.io/allure/](https://docs.qameta.io/allure/)

## What is the scope of the tests? <a name="seleniumScope"></a>
the tests that run with this selenium framework can be found in 

- SFO: [TestRail](https://airmapio.testrail.com/index.php?/suites/view/492) (Minh, please create a TestRail project for SFO)  
- WebApp: [TestRail](https://airmapio.testrail.com/index.php?/suites/view/492) (Minh, please create a TestRail project for WebApp)  
  
## What do you need to run the tests locally?  <a name="seleniumLocalReq"></a>
  
  - Java SDK 12  
 - Maven 3.X  
  
## How to run the tests locally?  <a name="seleniumLocalSteps"></a> 
You only have to run a maven command as follows:
  
    mvn clean verify -Dthreads=1 -Dremote=false -Dheadless=false  -DseleniumGridURL=http://127.0.0.1:4444/wd/hub -Denvironment=stage -Dbrowser=chrome   

 - mvn : runs maven  
 - clean: clean the target folder for compiled classes and tests results. this also cleans the screenshots folder.  
 - verify: makes TestNG run tests.  
 - -Dthreads : sets how many tests you want to run at the same time. this consumes hardware resources.  
-  -Dremote : sets if the tests run locally or inside a selenium grid. h
  
when the tests finish running, you must create a report running

    allure serve    

## How to run the tests in jenkins ?  <a name="seleniumRemoteSteps"></a>

<div style="text-align: left"><img src="readmeSupportAndDocs/seleniumJob1.png" width="600" height="300"></div>

when the job finish, you can check the report opening any allure link in
jenkins

<div style="text-align: left"><img src="readmeSupportAndDocs/allureReport1.png" width="600" height="300"></div>



## Where are the test code? <a name="seleniumTestCode"></a>
the test are located inside the class basicIT.java you can see the
notation @test before each one.
  
# JMeter Api Test <a name="jmeterMain"></a>

The **Apache JMeter™** application is open source software, a 100% pure
Java application designed to load test functional behavior and measure
performance. It was originally designed for testing Web Applications but
has since expanded to other test functions.

## What is the scope of the tests? <a name="jmeterScope"></a>
the tests that run with jmeter can be found in

- [jmeterIntegrationApi](https://jenkins.ops.airmap.com/job/TEST/view/JMETER%20TESTS/job/jmeterIntegrationTests/)  
(It contains tests of the different endpoints and validates the communication between them) [TestRail](https://airmapio.testrail.com/index.php?/suites/view/492).  
- [jmeterAdvisoriesApi](https://jenkins.ops.airmap.com/job/TEST/view/JMETER%20TESTS/job/jmeterAdvisoriesApi/)  
(It contains tests for advisories api v2) [TestRail](https://airmapio.testrail.com/index.php?/suites/view/493).  
- [jmeterAirspaceApi](https://jenkins.ops.airmap.com/job/TEST/view/JMETER%20TESTS/job/jmeterAirspaceApi/)  
(It contains tests for airspace api) [TestRail](https://airmapio.testrail.com/index.php?/suites/view/494).  
- [jmeterPilotApi](https://jenkins.ops.airmap.com/job/TEST/view/JMETER%20TESTS/job/jmeterPilotApi/)  
(It contains tests for pilot api) [TestRail](https://airmapio.testrail.com/index.php?/suites/view/495).  
- [jmeterCoreElevationApi](https://jenkins.ops.airmap.com/job/TEST/view/JMETER%20TESTS/job/jmeterCoreElevationApi/)  
(It contains tests for core elevation api) [TestRail](https://airmapio.testrail.com/index.php?/suites/view/496).
  
## What do you need to run the tests locally?  <a name="jmeterLocalReq"></a>

  - Java SDK 12  
 - Maven 3.X  
 - Apache JMeter (you can download the last version from [https://jmeter.apache.org/download_jmeter.cgi](https://jmeter.apache.org/download_jmeter.cgi))
## How to run the tests locally ?    <a name="jmeterLocalSteps"></a>
First, you have to create the bearer and pilot keys that the jmeter tests uses for auth with the API's  
to achieve this, you only have to run a maven command as follows:  

    mvn clean verify -Dthreads=1 -Dremote=false -Dheadless=false  -DseleniumGridURL=http://127.0.0.1:4444/wd/hub -Denvironment=stage -Dbrowser=chrome -Dit.test=BasicIT#getBearerUpdate 

- mvn : runs maven  
 - clean: clean the target folder for compiled classes and tests results. this also cleans the screenshots folder.  
 - verify: makes TestNG run tests.  
 - -Dthreads : sets how many tests you want to run at the same time. this consumes hardware resources.  
 - -Dremote : sets if the tests run locally or inside a selenium grid.  
 - -Dit.test: sets a particular test to run. in this case, we run the test that generates the credentials

 this will create two files:

    bearer.txt
	pilot.txt
this are the files that jmeter uses to load the user credentials. 
Second, you have to open the .jmx file that contains the testsuite that you want to run (you can use jmeter with GUI) 
  
## How to run the tests in jenkins ?  <a name="jmeterRemoteSteps"></a>

1. Once inside the job you want to execute, you have to click on "build
   with parameters"
<div style="text-align: left"><img src="readmeSupportAndDocs/jmeterInJenkins1.png" width="600" height="300"></div>

2. Now, you have to select in which environment you want to run the
   tests.
<div style="text-align: left"><img src="readmeSupportAndDocs/jmeterInJenkins2.png" width="600" height="300"></div>

3. The tests begin to run. you can see the real-time work executed in the jenkins console output.  
<div style="text-align: left"><img src="readmeSupportAndDocs/jmeterInJenkins3.png" width="600" height="300"></div>

4. Once finished, a report is made that can be downloaded from Jenkins. the link is published in slack, airmap-qa group. it has to be extracted and inside the folder, you find the index.html file that shows you the results.  
<div style="text-align: left"><img src="readmeSupportAndDocs/jmeterInJenkins4.png" width="600" height="300"></div>

<div style="text-align: left"><img src="readmeSupportAndDocs/jmeterInJenkins5.png" width="600" height="300"></div>


## Where are the test code? 
the test are located inside the folder jmeterFiles. the are named by endpoint and environment, for example:

    advisoriesApiOrca.jmx
this file is going to run all the advisories endpoint tests in the orca enviroment. 